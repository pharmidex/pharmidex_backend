﻿using Pharmidex.Core.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace PharmidexData.Identities
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message, string attachment)
        {
            if (GlobalConfig.IsLocalServer)
                message.Destination = GlobalConfig.DeveloperEmail;
            string fromAddress = GlobalConfig.ProjectName + "<" + GlobalConfig.SMTP_USERNAME + ">";
            using (MailMessage mailMessage = new MailMessage(fromAddress, message.Destination))
            {
                mailMessage.Subject = message.Subject;
                mailMessage.Body = message.Body;
                mailMessage.IsBodyHtml = true;

                if (!string.IsNullOrEmpty(attachment))
                {
                    mailMessage.Attachments.Add(new Attachment(attachment));
                }

                SmtpClient smtp = new SmtpClient
                {
                    Host = GlobalConfig.SMTP_HOST,
                    EnableSsl = true,
                };
                NetworkCredential networkCredential = new NetworkCredential(GlobalConfig.SMTP_USERNAME, GlobalConfig.SMTP_PASSWORD);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = networkCredential;
                smtp.Port = GlobalConfig.SMTPPort;
                try
                {
                    await smtp.SendMailAsync(mailMessage);
                }
                catch
                {
                }
            }
        }

        public async Task SendAsync(IdentityMessage message)
        {
            await SendAsync(message, "");
        }
    }
}
