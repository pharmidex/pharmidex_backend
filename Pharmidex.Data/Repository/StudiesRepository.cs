﻿using AmarnaEnergyData.IRepository;
using Pharmidex.Core.Enums;
using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using Pharmidex.Core.Resources;
using Pharmidex.Entity;
using PharmidexData.Identities;
using PharmidexData.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Text;
using System.Threading.Tasks;

namespace AmarnaEnergyData.Repository
{
    public class StudiesRepository : GenericRepository<Studies>, IStudiesRepository
    {
        private readonly ApplicationDbContext _ctx;

        public StudiesRepository(ApplicationDbContext ctx) : base(ctx)
        {
            _ctx = ctx;
        }

        public async Task<ResponseModel> AddUpdate(ManageStudiesModel model)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            try
            {
                bool isNew = false;

                Studies studies = await _ctx.Studies.FirstOrDefaultAsync(x => x.Id == model.Id);
                if (isNew = studies == null)
                    studies = new Studies() { StudyDetails = new List<StudyDetails>() };

                studies.ProposalDate = model.ProposalDate;
                studies.CustomerId = model.CustomerId;
                studies.ProposalNo = model.ProposalNo;
                studies.UId = model.UId;
                _ctx.StudyDetails.RemoveRange(studies.StudyDetails);
                foreach (var item in model.StudyDetails)
                {
                    studies.StudyDetails.Add(new StudyDetails()
                    {
                        DepartmentId = item.DepartmentId,
                        DirectorId = item.DirectorId,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceNo = item.InvoiceNo,
                        Notes = item.Notes,
                        ReportDate = item.ReportDate,
                        SamplesReceivedDate = item.SamplesReceivedDate,
                        StudyStatus = item.StudyStatus,
                        StudyTypeId = item.StudyTypeId,
                        VIVOEndDate = item.VIVOEndDate,
                        VIVOStartDate = item.VIVOStartDate,
                        CompletionAuthorisedBy = item.CompletionAuthorisedBy,
                        Compounds = item.Compounds,
                        BAStartDate = item.BAStartDate,
                        CompletionDate = item.CompletionDate,
                        BAEndDate = item.BAEndDate,
                        StudyNo = "PSN19-" + Utilities.GetRandomAlphaNumeric(5),
                        CompoundsRecDate = item.CompoundsRecDate,
                        SamplesShippedDate = item.SamplesReceivedDate,
                        SamplesDescription = item.SamplesDescription,
                        DateArchived = item.DateArchived,
                    });
                }

                if (isNew)
                {
                    _ctx.Studies.Add(studies);
                }
                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = isNew ? ResponseMessages.StudyAdded : ResponseMessages.StudyUpdated;
            }
            catch (DbEntityValidationException err)
            {
                StringBuilder sb = new StringBuilder();
                foreach (DbEntityValidationResult eve in err.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        sb.Append($"Property: {ve.PropertyName}, Error: {ve.ErrorMessage} {Environment.NewLine}");
                    }
                }
                mResult.Message = sb.ToString();
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }
            return mResult;
        }

    }
}