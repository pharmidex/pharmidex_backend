﻿using PharmidexData.Identities;
using PharmidexData.IRepository;
using PharmidexEntity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace PharmidexData.Repository
{
    public class UserRepository : GenericRepository<ApplicationUser>, IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _context = dbContext;
        }

        public IDbSet<IdentityRole> GetRoles()
        {
            return _context.Roles;
        }
    }
}