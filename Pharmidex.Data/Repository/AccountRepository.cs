﻿using Pharmidex.Core.Enums;
using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using PharmidexData.Identities;
using PharmidexData.IRepository;
using PharmidexEntity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace PharmidexData.Repository
{
    public class AccountRepository : IAccountRepository, IDisposable
    {
        private readonly ApplicationUserManager _userManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly ApplicationDbContext _ctx;

        public AccountRepository(ApplicationDbContext dbContext)
        {
            _ctx = dbContext;
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx));
        }

        public AccountRepository(ApplicationDbContext dbContext, IAuthenticationManager authenticationManager)
        {
            _ctx = dbContext;
            _authenticationManager = authenticationManager;
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx));
        }

        public async Task<ApplicationUser> UserAuthenticate(SigninRequestModel model)
        {
            return await _userManager.FindByEmailAsync(model.Email);
        }

        public async Task CreateIdentityAsync(ApplicationUser User, UserRole Role)
        {
            ClaimsIdentity userIdentity = await _userManager.CreateIdentityAsync(User, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim(ClaimTypes.Role, Role.ToString()));

            if (!string.IsNullOrEmpty(User.Name))
                userIdentity.AddClaim(new Claim("Name", User.Name));

            if (!string.IsNullOrEmpty(User.Email))
                userIdentity.AddClaim(new Claim("Email", User.Email));

            _authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, userIdentity);
        }

        public void IdentitySignout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie, DefaultAuthenticationTypes.TwoFactorCookie);
        }

        public async Task<ApplicationUser> FindByEmailAsync(IUserEmail model)
        {
            return await _userManager.FindByEmailAsync(model.Email);
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser User, string Password)
        {
            if (string.IsNullOrEmpty(Password))
                return await _userManager.CreateAsync(User);
            else
                return await _userManager.CreateAsync(User, Password);
        }

        public async Task<IdentityResult> AddToRoleAsync(RoleRequestModel model)
        {
            return await _userManager.AddToRoleAsync(model.UId, model.UserRole.ToString());
        }

        public async Task CreateIdentityAsync(ApplicationUser User, bool isPersistent, bool RememberBrowser)
        {
            ClaimsIdentity userIdentity = await _userManager.CreateIdentityAsync(User, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("Name", User.Name));
            userIdentity.AddClaim(new Claim("Email", User.Email));
            _authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, userIdentity);
        }

        public async Task<bool> CheckPasswordAsync(ApplicationUser User, string Password)
        {
            return await _userManager.CheckPasswordAsync(User, Password);
        }

        public async Task<bool> IsInRoleAsync(RoleRequestModel model)
        {
            return await _userManager.IsInRoleAsync(model.UId, model.UserRole.ToString());
        }

        public async Task SaveSigninLog(IUserSinginLogModel model)
        {
            string IpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            DateTime currentDateTime = Utilities.GetSystemDateTimeUTC();
            if (model.IsSignout)
            {
                List<ApplicationUserLogin> entities = _ctx.ApplicationUserLogin.Where(x => x.Id.Equals(IpAddress)).ToList();

                if (entities != null && entities.Any())
                {
                    entities.ForEach(entity => entity.LogoutUTC = currentDateTime);
                    await _ctx.SaveChangesAsync();
                }
            }
            else
            {
                ApplicationUserLogin entity = new ApplicationUserLogin()
                {
                    Platform = model.Platform,
                    UId = model.UId,
                    IP = IpAddress,
                    LoginUTC = currentDateTime,
                };
                _ctx.ApplicationUserLogin.Add(entity);
                await _ctx.SaveChangesAsync();
            }
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(IUserId model)
        {
            DpapiDataProtectionProvider provider = new DpapiDataProtectionProvider(GlobalConfig.ProjectName);
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("EmailConfirmation"));
            return await _userManager.GenerateEmailConfirmationTokenAsync(model.UId);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(ConfirmEmailRequestModel model)
        {
            IdentityResult IdentityResult;
            DpapiDataProtectionProvider provider = new DpapiDataProtectionProvider(GlobalConfig.ProjectName);
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("EmailConfirmation"));

            IdentityResult = await _userManager.ConfirmEmailAsync(model.UId, model.Token);
            _userManager.UpdateSecurityStamp(model.UId);
            return IdentityResult;
        }

        public async Task<IdentityResult> ChangePasswordAsync(IChangePasswordRequestModel model)
        {
            DpapiDataProtectionProvider provider = new DpapiDataProtectionProvider(GlobalConfig.ProjectName);
            _userManager.UserTokenProvider =
                new DataProtectorTokenProvider<ApplicationUser>(provider.Create("ChangePassword"));
            return await _userManager.ChangePasswordAsync(model.UId, model.CurrentPassword, model.NewPassword);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(IUserId model)
        {
            DpapiDataProtectionProvider provider = new DpapiDataProtectionProvider(GlobalConfig.ProjectName);
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("PasswordReset"));
            return await _userManager.GeneratePasswordResetTokenAsync(model.UId);
        }

        public async Task<IdentityResult> ResetPasswordAsync(IResetPasswordRequestModel model)
        {
            IdentityResult IdentityResult;
            DpapiDataProtectionProvider provider = new DpapiDataProtectionProvider(GlobalConfig.ProjectName);
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("PasswordReset"));
            IdentityResult = await _userManager.ResetPasswordAsync(model.UId, model.PasswordResetToken, model.Password);
            _userManager.UpdateSecurityStamp(model.UId);
            return IdentityResult;
        }

        public async Task<ApplicationUser> FindAsync(UserLoginInfo userLoginInfo)
        {
            return await _userManager.FindAsync(userLoginInfo);
        }

        public async Task<IdentityResult> AddLoginAsync(AuthenticateResultModel model, UserLoginInfo userLoginInfo)
        {
            return await _userManager.AddLoginAsync(model.UId, userLoginInfo);
        }

        public async Task<ApplicationUser> FindByIdAsync(string UserId)
        {
            return await _userManager.FindByIdAsync(UserId);
        }

        public async Task<ApplicationUser> GetUserProfileAsync(IUserId model)
        {
            return await _ctx.Users.FirstOrDefaultAsync(x => x.Id == model.UId);
        }

        public async Task<IdentityResult> ChangePasswordWithoutToken(ApplicationUser user, string Password)
        {
            user.PasswordHash = _userManager.PasswordHasher.HashPassword(Password);
            //update user password
            IdentityResult IdentityResult = await _userManager.UpdateAsync(user);
            return IdentityResult;
        }
        public async Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {
            return await _userManager.UpdateAsync(user);
        }

        public void Dispose()
        {
            _userManager.Dispose();
            _ctx.Dispose();
        }
    }
}