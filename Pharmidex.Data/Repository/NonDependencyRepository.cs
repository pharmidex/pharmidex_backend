﻿using Pharmidex.Core.Enums;
using Pharmidex.Core.Models;
using PharmidexData.Identities;
using PharmidexEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace PharmidexData.Repository
{
    public class NonDependencyRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public NonDependencyRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<ApplicationSettings> GetApplicationSettingAsync()
        {
            return await _ctx.ApplicationSettings.FirstOrDefaultAsync();
        }

        public async Task<UserAuthorizeFilterModel> FindByIdAsync(string UserId)
        {
            UserAuthorizeFilterModel result = await (from u in _ctx.Users
                                                     from r in u.Roles
                                                     join roles in _ctx.Roles on r.RoleId equals roles.Id into G_roles
                                                     where u.Id == UserId
                                                     select new UserAuthorizeFilterModel
                                                     {
                                                         UId = u.Id,
                                                         Name = u.Name,
                                                         Email = u.Email,
                                                         EmailConfirmed = u.EmailConfirmed,
                                                         IsActive = u.IsActive,
                                                         RoleNames = (from rr in G_roles
                                                                      select rr.Name).ToList()
                                                     }).FirstOrDefaultAsync();
            if (result != null)
            {
                result.UserRoles = (from r in result.RoleNames
                                    select (UserRole)Enum.Parse(typeof(UserRole), r)).ToList();
            }
            return result;
        }

        public async Task<List<DropdownListItemModel>> GetDepartment()
        {
            return await (from x in _ctx.Department
                          orderby x.Title
                          select new DropdownListItemModel()
                          {
                              Value = x.Id.ToString(),
                              Text = x.Title,
                          }).ToListAsync();
        }

        public async Task<List<DropdownListItemModel>> GetStudyType()
        {
            return await (from x in _ctx.StudyType
                          orderby x.Title
                          select new DropdownListItemModel()
                          {
                              Value = x.Id.ToString(),
                              Text = x.Title,
                          }).ToListAsync();
        }
        public async Task<List<DropdownListItemModel>> GetDirector()
        {
            return await (from x in _ctx.Director
                          orderby x.Title
                          select new DropdownListItemModel()
                          {
                              Value = x.Id.ToString(),
                              Text = x.Title,
                          }).ToListAsync();
        }

        public async Task<List<DropdownListItemModel>> GetCustomers()
        {
            return await (from x in _ctx.Users
                          orderby x.Name
                          select new DropdownListItemModel()
                          {
                              Value = x.Id,
                              Text = x.Name,
                          }).ToListAsync();
        }


        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}