﻿using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models.Base;
using PharmidexData.Identities;
using PharmidexData.IRepository;
using PharmidexEntity;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmidexData.Repository
{
    public class ApplicationSettingRepository : GenericRepository<ApplicationSettings>, IApplicationSettingRepository
    {
        private readonly ApplicationDbContext _context;

        public ApplicationSettingRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _context = dbContext;
        }        
    }
}