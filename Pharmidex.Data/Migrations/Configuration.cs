﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pharmidex.Core.Enums;
using Pharmidex.Entity;
using PharmidexData.Identities;
using PharmidexEntity;
using System.Data.Entity.Migrations;
using System.Linq;
namespace PharmidexData.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            ApplicationRoleManager roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context));

            if (context.ApplicationSettings.Any() == false)
            {
                context.ApplicationSettings.AddOrUpdate(x => x.Id,
                    new ApplicationSettings { Id = 1, AndroidVersion = 1, iOSVersion = 1, ForceUpdateAndroid = false, ForceUpdateiOS = false });
            }
            if (context.Department.Any() == false)
            {
                context.Department.AddOrUpdate(x => x.Id,
                    new Department { Id = 1, Title = "Test Department" });
            }
            if (context.StudyType.Any() == false)
            {
                context.StudyType.AddOrUpdate(x => x.Id,
                    new StudyType { Id = 1, Title = "Test StudyType" });
            }
            if (context.Director.Any() == false)
            {
                context.Director.AddOrUpdate(x => x.Id,
                    new Director { Id = 1, Title = "Test Director" });
            }

            #region Add Roles
            string userRole = UserRole.Admin.ToString();
            {
                IdentityRole role = roleManager.FindByNameAsync(userRole).Result;
                if (role == null)
                {
                    role = new IdentityRole(userRole);
                    roleManager.Create(role);
                }
            }
            userRole = UserRole.Customer.ToString();
            {
                IdentityRole role = roleManager.FindByNameAsync(userRole).Result;
                if (role == null)
                {
                    role = new IdentityRole(userRole);
                    roleManager.Create(role);
                }
            }
            #endregion

            #region Add admin
            {
                const string emailId = "admin@Pharmidex.com";
                const string userName = "Admin";
                const string password = "Ph#rma$22";

                ApplicationUser user = userManager.FindByName(userName);
                if (user == null)
                {
                    userRole = UserRole.Admin.ToString();

                    user = new ApplicationUser()
                    {
                        Name = "Admin",
                        UserName = userName,
                        Email = emailId,
                        EmailConfirmed = true,
                        IsActive = true,
                    };
                    IdentityResult result = userManager.Create(user, password);
                    if (result.Succeeded)
                    {
                        userManager.AddToRole(user.Id, userRole);
                        userManager.SetLockoutEnabled(user.Id, false);
                    }
                }
            }
            #endregion           
        }
    }
}