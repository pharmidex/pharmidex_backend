﻿using PharmidexEntity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmidexData.IRepository
{
    public interface IUserRepository : IGenericRepository<ApplicationUser>
    {
        IDbSet<IdentityRole> GetRoles();
    }
}