﻿using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using Pharmidex.Entity;
using PharmidexData.IRepository;
using System.Threading.Tasks;

namespace AmarnaEnergyData.IRepository
{
    public interface IStudiesRepository : IGenericRepository<Studies>
    {
        Task<ResponseModel> AddUpdate(ManageStudiesModel model);
    }
}