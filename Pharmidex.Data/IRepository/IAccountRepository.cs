﻿using Pharmidex.Core.Enums;
using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using PharmidexEntity;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;

namespace PharmidexData.IRepository
{
    public interface IAccountRepository : IDisposable
    {
        Task<ApplicationUser> UserAuthenticate(SigninRequestModel model);
        Task CreateIdentityAsync(ApplicationUser user, UserRole Role);
        void IdentitySignout();
        Task<ApplicationUser> FindByEmailAsync(IUserEmail model);
        Task<IdentityResult> CreateAsync(ApplicationUser user, string password);
        Task<IdentityResult> AddToRoleAsync(RoleRequestModel addRoleRequestModel);
        Task CreateIdentityAsync(ApplicationUser user, bool isPersistent, bool rememberBrowser);
        Task<bool> CheckPasswordAsync(ApplicationUser user, string password);
        Task<bool> IsInRoleAsync(RoleRequestModel roleRequestModel);
        Task SaveSigninLog(IUserSinginLogModel model);
        Task<string> GenerateEmailConfirmationTokenAsync(IUserId model);
        Task<string> GeneratePasswordResetTokenAsync(IUserId model);
        Task<IdentityResult> ConfirmEmailAsync(ConfirmEmailRequestModel model);
        Task<IdentityResult> ResetPasswordAsync(IResetPasswordRequestModel model);
        Task<ApplicationUser> FindAsync(UserLoginInfo userLoginInfo);
        Task<IdentityResult> AddLoginAsync(AuthenticateResultModel authenticateResultModel, UserLoginInfo userLoginInfo);
        Task<ApplicationUser> FindByIdAsync(string UserId);
        Task<ApplicationUser> GetUserProfileAsync(IUserId model);
        Task<IdentityResult> ChangePasswordAsync(IChangePasswordRequestModel model);
        Task<IdentityResult> ChangePasswordWithoutToken(ApplicationUser user, string Password);
        Task<IdentityResult> UpdateAsync(ApplicationUser user);
    }
}