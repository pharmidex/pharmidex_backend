﻿using PharmidexEntity;

namespace PharmidexData.IRepository
{
    public interface IApplicationSettingRepository : IGenericRepository<ApplicationSettings>
    {        
    }
}