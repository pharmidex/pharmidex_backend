﻿using Pharmidex.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PharmidexData.IRepository
{
    public interface IGenericRepository<TEntity> : IDisposable
    {
        Task<TEntity> Add(TEntity entity);

        Task Edit(TEntity entity);

        Task AddRange(List<TEntity> entities);

        Task<int> Delete(TEntity baseEntity);

        Task<bool> Delete(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> Find(IBaseEntityId baseEntity);

        IQueryable<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate);
    }
}