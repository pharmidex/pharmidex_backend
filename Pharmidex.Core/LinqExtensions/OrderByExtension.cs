﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Pharmidex.Core.LinqExtensions
{
    public static class OrderByExtension
    {
        public static IOrderedQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> query, string memberName, bool asc = true)
        {
            ParameterExpression[] typeParams = new ParameterExpression[] {
                Expression.Parameter(typeof(TEntity), "")
            };

            PropertyInfo pi = typeof(TEntity).GetProperty(memberName);

            return (IOrderedQueryable<TEntity>)query.Provider.CreateQuery(
                Expression.Call(
                    typeof(Queryable),
                    asc ? "OrderBy" : "OrderByDescending",
                    new Type[] { typeof(TEntity), pi.PropertyType },
                    query.Expression,
                    Expression.Lambda(Expression.Property(typeParams[0], pi), typeParams))
                );
        }
    }
}