﻿using Pharmidex.Core.Models;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Pharmidex.Core.LinqExtensions
{
    public static class DynamicPredicateExtension
    {
        public static Expression<Func<TEntity, bool>> GetPredicates<TEntity>(DataTableAjaxPostModel model)
        {
            Expression<Func<TEntity, bool>> predicates = null;

            string searchValues = (model.search != null) ? model.search.value : string.Empty;

            if (!string.IsNullOrWhiteSpace(searchValues))
            {
                predicates = PredicateBuilder.New<TEntity>();

                List<string> searchValList = searchValues.Split(' ')
                    .Where(x => !string.IsNullOrWhiteSpace(x)).ToList().ConvertAll(x => x.ToLower());

                List<string> searchableFields = model.columns
                    .Where(x => x.data != null && x.searchable).Select(x => x.data).ToList();

                foreach (string fieldName in searchableFields)
                {
                    ParameterExpression parameter = Expression.Parameter(typeof(TEntity));

                    MemberExpression property = Expression.Property(parameter, fieldName);

                    Type propertyType = ((PropertyInfo)property.Member).PropertyType;

                    Expression<Func<TEntity, bool>> predicate = null;

                    MethodInfo containsMethod = searchValues.GetType().GetMethod("Contains");
                    foreach (string searchVal in searchValList)
                    {
                        int intResult = 0;
                        if ((propertyType.Name.ToLower() == "int32") && int.TryParse(searchVal, out intResult))
                        {
                            Expression constant = Expression.Constant(intResult);
                            BinaryExpression callEqual = Expression.Equal(property, constant);
                            predicate = Expression.Lambda<Func<TEntity, bool>>(callEqual, parameter);
                        }
                        else if (propertyType.Name.ToLower() == "string")
                        {
                            Expression constant = Expression.Constant(searchVal);
                            MethodCallExpression callContains = Expression.Call(property, containsMethod, constant);
                            predicate = Expression.Lambda<Func<TEntity, bool>>(callContains, parameter);
                        }
                        if (predicate != null)
                            predicates = predicates.Or(predicate);
                    }
                }
            }
            return predicates;
        }
    }
}