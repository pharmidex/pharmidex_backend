﻿using Pharmidex.Core.Enums;
using Pharmidex.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Pharmidex.Core.Helpers
{
    public static class Utilities
    {
        public static DateTime GetSystemDateTimeUTC()
        {
            return DateTime.UtcNow;
        }

        public static string GetRandomAlphaNumeric(int length)
        {
            Random random = new Random();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(chars.Select(c => chars[random.Next(chars.Length)]).Take(length).ToArray());
        }

        public static async Task<string> GenerateEmailBodyAsync(EmailTemplate templateFileName, Dictionary<string, string> dicPlaceholders)
        {
            dicPlaceholders.Add("{{projectName}}", GlobalConfig.ProjectName);
            string templatePath = Path.Combine(GlobalConfig.EmailTemplatePath, templateFileName.ToString() + ".html");
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(templatePath))
            {
                body = await reader.ReadToEndAsync();
            }
            if (body.Length > 0)
            {
                foreach (KeyValuePair<string, string> item in dicPlaceholders)
                {
                    body = body.Replace(item.Key, item.Value);
                }
            }
            return body;
        }

        public static void WriteLog(string Content, string fileName = "Logs.txt")
        {
            try
            {
                string logFileName = Path.Combine(GlobalConfig.LogPath, fileName);
                if (!File.Exists(logFileName))
                    File.CreateText(logFileName);
                StreamWriter sw = new StreamWriter(logFileName, true);
                sw.WriteLine(string.Format("**************"));
                sw.WriteLine(Content);
                sw.WriteLine("*************************");
                sw.Close();
            }
            catch (Exception)
            {
            }
        }

        public static string ToFirstCharUpper(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return string.Empty;
            else if (input.Length > 1)
                return char.ToUpper(input[0]) + input.Substring(1).ToLower();
            else
                return input.ToUpper();
        }
    }
}