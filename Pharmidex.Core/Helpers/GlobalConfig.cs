﻿using System;
using System.Configuration;

namespace Pharmidex.Core.Helpers
{
    public static class GlobalConfig
    {
        public const int AccessTokenExpireInMinutes = 525600;
        public const string ProjectName = "Pharmidex";
        public const string AdminProjectName = "Pharmidex - Admin";

        public static string BreakRow = "</br>";

        public static string asc = "asc";
        public static string desc = "desc";

        public const string DateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        public const int PageSize = 10;
        public static string DefaultImage = "default.png";

        public static string ExcelFileName = ConfigurationManager.AppSettings["ExcelFileName"].ToString();

        public static string BasePath = ConfigurationManager.AppSettings["BasePath"].ToString();
        public static string BaseUrl = ConfigurationManager.AppSettings["BaseRouteUrl"].ToString();
        public static bool IsLocalServer = bool.Parse(ConfigurationManager.AppSettings["IsLocalServer"]);
        public static string AdminBaseUrl = BaseUrl + "Admin/";

        public static string FileBasePath = $@"{BasePath}Files\";
        public static string FileBaseUrl = $@"{BaseUrl}Files/";

        public static string ExcelPath = $@"{FileBasePath}Excel\";
        public static string ExcelUrl = $@"{FileBaseUrl}Excel/";

        public static string UserProfilePath = $@"{FileBasePath}UserImages\";
        public static string UserProfileUrl = $@"{FileBaseUrl}UserImages/";

        public static string EmailTemplatePath = $@"{FileBasePath}EmailTemplates\";
        public static string EmailTemplateUrl = $@"{FileBaseUrl}EmailTemplates/";

        public static string LogPath = FileBasePath + @"Log\";

        public static string ClientEmail = ConfigurationManager.AppSettings["ClientEmail"].ToString();
        public static string ClientName = ConfigurationManager.AppSettings["ClientName"].ToString();
        public static string SMTP_USERNAME = ConfigurationManager.AppSettings["SMTP_USERNAME"].ToString();
        public static string SMTP_PASSWORD = ConfigurationManager.AppSettings["SMTP_PASSWORD"].ToString();
        public static string SMTP_HOST = ConfigurationManager.AppSettings["HOST"].ToString();
        public static int SMTPPort = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());

        //SEAI
        public static string baseApimUrl = ConfigurationManager.AppSettings["baseApimUrl"].ToString();
        public static string TPAPIBaseUrl = ConfigurationManager.AppSettings["TPAPIBaseUrl"].ToString();

        public static string AuthUrl = ConfigurationManager.AppSettings["AuthUrl"].ToString();
        public static string ClientId = ConfigurationManager.AppSettings["ClientId"].ToString();
        public static string ClientSecret = ConfigurationManager.AppSettings["ClientSecret"].ToString();
        public static string GrantType = ConfigurationManager.AppSettings["GrantType"].ToString();
        public static string TPID = ConfigurationManager.AppSettings["TPID"].ToString();
        public static string Scope = ConfigurationManager.AppSettings["Scope"].ToString();

        public static string DeveloperEmail = "jafar.imperoit@gmail.com";
    }
}