﻿using Pharmidex.Core.Enums;
using System.Collections.Generic;

namespace Pharmidex.Core.Models
{
    public class UserAuthorizeFilterModel : IUserId, IUserName, IUserEmail
    {
        public string UId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool IsActive { get; set; }
        public List<UserRole> UserRoles { get; set; }
        public List<string> RoleNames { get; set; }
    }
}