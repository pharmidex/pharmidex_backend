﻿namespace Pharmidex.Core.Models
{
    public interface IPagination
    {
        int PageIndex { get; set; }
        int PageSize { get; set; }
    }
}