﻿using Pharmidex.Core.Enums;

namespace Pharmidex.Core.Models
{
    public interface IUserId
    {
        string UId { get; set; }
    }

    public interface IUserName
    {
        string Name { get; set; }
    }

    public interface IUserEmail
    {
        string Email { get; set; }
    }

    public interface ICountryCode
    {
        string CountryCode { get; set; }
    }

    public interface IUserNameEmail
    {
        string Name { get; set; }
        string Email { get; set; }
    }

    public interface IUserRole
    {
        UserRole UserRole { get; set; }
    }

    public interface IPlatform
    {
        Platform Platform { get; set; }
    }
}