﻿using Pharmidex.Core.Enums;

namespace Pharmidex.Core.Models
{
    public interface IUserSinginLogModel : IUserId
    {
        Platform Platform { get; set; }
        bool IsSignout { get; set; }
    }

    public interface IUserAuthenticateRequest : IUserEmail
    {
        string Password { get; set; }
    }

    public interface IResetPasswordRequestModel : IUserAuthenticateRequest, IUserId
    {
        string PasswordResetToken { get; set; }
        string ConfirmPassword { get; set; }
    }

    public interface ISignoutRequestModel : IUserSinginLogModel
    {
    }

    public interface IChangePasswordRequestModel : IUserId
    {
        string CurrentPassword { get; set; }
        string NewPassword { get; set; }
        string ConfirmPassword { get; set; }
    }
}