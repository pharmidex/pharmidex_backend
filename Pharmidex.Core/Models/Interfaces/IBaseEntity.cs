﻿using System;

namespace Pharmidex.Core.Models
{
    public interface IBaseEntityId
    {
        int Id { get; set; }
    }

    public interface IBaseEntity : IBaseEntityId
    {
        DateTime CreatedUTC { get; set; }
    }

    public class IBaseIdModel : IBaseEntityId
    {
        public int Id { get; set; }
        public DateTime CreatedUTC { get; set; }
    }
}