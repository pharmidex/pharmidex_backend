﻿using Pharmidex.Core.Enums;
using System.ComponentModel.DataAnnotations;

namespace Pharmidex.Core.Models
{
    public class ForgotPasswordRequestModel : IUserEmail
    {
        [Required(ErrorMessage = "Please enter email")]
        [EmailAddress(ErrorMessage = "Please enter valid email address")]
        public string Email { get; set; }
    }

    public class ForgotPasswordRequestModelAdmin : ForgotPasswordRequestModel, IUserRole, IPlatform
    {
        public UserRole UserRole { get; set; }
        public Platform Platform { get; set; }
    }

    public class UserIdModel : IUserId
    {
        public virtual string UId { get; set; }
    }
    public class SigninRequestModel : UserSinginLogModel, IUserEmail
    {
        [Required(ErrorMessage = "Please enter email!")]
        [EmailAddress(ErrorMessage = "Please enter valid email address!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter password")]
        [StringLength(maximumLength: 16, ErrorMessage = "Password length must be {2} to {1} characters long", MinimumLength = 8)]
        public string Password { get; set; }
    }

    public class SignupModel : SigninRequestModel, ICountryCode
    {
        [Required(ErrorMessage = "Please enter {0}!")]
        [StringLength(200, ErrorMessage = "Name length must be between {2} to {1} characters long", MinimumLength = 1)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please provide {0}!")]
        public string CountryCode { get; set; }

        [Required(ErrorMessage = "Please enter {0}!")]
        public string PostCode { get; set; }

        public string PhoneNumber { get; set; }
    }

    public class AuthenticateResultModel : UserIdModel
    {
        public string Name { get; set; }
    }

    public class ConfirmEmailRequestModel : UserIdModel
    {
        public string Email { get; set; }
        public string Token { get; set; }
    }

    public class SigninResultModel : UserIdModel
    {
        public BearerAccessTokenResultModel AccessToken { get; set; }
    }

    public class RoleRequestModel : UserIdModel, IUserRole
    {
        public UserRole UserRole { get; set; }
    }

    #region UserSinginLog    
    public class UserSinginLogModel : IUserSinginLogModel, IUserRole
    {
        public string FCMToken { get; set; }
        public bool IsSignout { get; set; }
        public decimal AppVr { get; set; }
        public string DeviceMf { get; set; }
        public string DeviceModel { get; set; }
        public string UId { get; set; }
        public UserRole UserRole { get; set; }

        [Required(ErrorMessage = "Please provide platform")]
        public Platform Platform { get; set; }
    }
    #endregion

    public class SignoutRequestModel : UserSinginLogModel
    { }

    #region ResetPassword
    public class ResetPasswordRequestModel : IResetPasswordRequestModel
    {
        [Required(ErrorMessage = "Please enter code")]
        [Display(Name = "Password reset token")]
        public string PasswordResetToken { get; set; }

        [Required(ErrorMessage = "Please enter password")]
        [StringLength(maximumLength: 16, ErrorMessage = "Password length must be {2} to {1} characters long", MinimumLength = 8)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm password")]
        [Compare("Password", ErrorMessage = "{0} does not match with password")]
        public string ConfirmPassword { get; set; }

        public string Email { get; set; }
        public string UId { get; set; }
    }
    #endregion ResetPassword

    #region ChangePassword
    public class ChangePasswordRequestModel : IChangePasswordRequestModel
    {
        public string UId { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(16, ErrorMessage = "{0} should be {2} to {1} characters long.", MinimumLength = 8)]
        [Display(Name = "Old password")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(16, ErrorMessage = "{0} should be {2} to {1} characters long.", MinimumLength = 8)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Compare("NewPassword", ErrorMessage = "{0} does not match with password")]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }
    }
    #endregion

    #region ResetPassword
    public class VerifyCodeRequestModel : IUserEmail
    {
        [Required(ErrorMessage = "Please enter code")]
        [Display(Name = "Password reset Code")]
        public string PasswordResetCode { get; set; }

        public string Email { get; set; }
        public string UId { get; set; }
    }
    #endregion ResetPassword
}
