﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Pharmidex.Core.Models
{
    public class BearerAccessTokenResultModel
    {
        public BearerAccessTokenResultModel()
        {
            error = string.Empty;
            accessToken = string.Empty;
            tokenType = string.Empty;
        }
        [JsonProperty("access_token")]
        public string accessToken { get; set; }
        [JsonProperty("token_type")]
        public string tokenType { get; set; }
        [JsonProperty("error")]
        public string error { get; set; }
    }

    public class RefreshTokenRequestModel
    {
        public string Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Subject { get; set; }
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiresUtc { get; set; }
        [Required]
        public string ProtectedTicket { get; set; }
    }
}
