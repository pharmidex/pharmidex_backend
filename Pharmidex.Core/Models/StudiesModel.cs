﻿using Pharmidex.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pharmidex.Core.Models
{
    public class ManageStudiesModel
    {
        [Required]
        public int Id { get; set; }
        public string UId { get; set; }

        [Required]
        public string CustomerId { get; set; }
        [Required]
        public string ProposalNo { get; set; }
        [Required]
        public DateTime ProposalDate { get; set; }
        public List<StudyDetailsManageModel> StudyDetails { get; set; }
    }
    public class StudyDetailsManageModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int DepartmentId { get; set; }

        [Required]
        public int StudyTypeId { get; set; }

        [Required]
        public int DirectorId { get; set; }

        public string StudyNo { get; set; }

        [Required]
        public string Compounds { get; set; }
        [Required]
        public DateTime CompoundsRecDate { get; set; }
        [Required]
        public DateTime VIVOStartDate { get; set; }
        [Required]
        public DateTime VIVOEndDate { get; set; }
        [Required]
        public DateTime SamplesReceivedDate { get; set; }
        [Required]
        public DateTime BAStartDate { get; set; }
        [Required]
        public DateTime BAEndDate { get; set; }
        [Required]
        public DateTime CompletionDate { get; set; }
        [Required]
        public DateTime ReportDate { get; set; }
        [Required]
        public string SamplesDescription { get; set; }
        [Required]
        public DateTime SamplesShippedDate { get; set; }
        [Required]
        public string CompletionAuthorisedBy { get; set; }
        [Required]
        public string Notes { get; set; }
        [Required]
        public StudyStatus StudyStatus { get; set; }

        [Required]
        public string InvoiceNo { get; set; }

        [Required]
        public DateTime InvoiceDate { get; set; }

        [Required]
        public DateTime DateArchived { get; set; }
    }

    public class StudiesDatatableModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string ProposalNo { get; set; }
        public DateTime ProposalDate { get; set; }
        public int Count { get; set; }
    }
}
