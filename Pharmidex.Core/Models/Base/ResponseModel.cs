﻿using Pharmidex.Core.Enums;

namespace Pharmidex.Core.Models.Base
{
    public interface IResponseModel
    {
        string Message { get; set; }
        ResponseStatus Status { get; set; }
    }
    public class ResponseModel : IResponseModel
    {
        public ResponseModel()
        {
            Status = ResponseStatus.Failed;
        }

        public string Message { get; set; }
        public ResponseStatus Status { get; set; }
    }

    public class ResponseModel<T> : ResponseModel
    {
        public T Result { get; set; }
    }

    public class UploadImageModel
    {
        public string ImageUrl { get; set; }
        public string ImageNameProperty { get; set; }
        public string SaveFilePath { get; set; }
    }

    public class SaveImageModel
    {
        public string ImgBase64String { get; set; }
        public string SaveFilePath { get; set; }
        public string Extension { get; set; }
        public string PreviousImageName { get; set; }
    }
}