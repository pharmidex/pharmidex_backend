﻿using Pharmidex.Core.Enums;
using Pharmidex.Core.Helpers;
using Pharmidex.Core.Resources;
using System.ComponentModel.DataAnnotations;

namespace Pharmidex.Core.Models.Base
{
    public class Pagination : IPagination, IUserId
    {
        public Pagination()
        {
            PageIndex = 1;
            PageSize = GlobalConfig.PageSize;
        }

        public string UId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ResponseMessages), ErrorMessageResourceName = "ProvidePageIndex")]
        public int PageIndex { get; set; }

        [Required(ErrorMessageResourceType = typeof(ResponseMessages), ErrorMessageResourceName = "ProvidePageSize")]
        public int PageSize { get; set; }
    }
}