﻿using Pharmidex.Core.Enums;

namespace Pharmidex.Core.Models.Base
{
    public enum ToastMessageStatus
    {
        Error,
        Success,
        Info,
        Warning
    }

    public class ToastMessageModel
    {
        public ToastMessageModel(IResponseModel model)
        {
            switch (model.Status)
            {
                case ResponseStatus.Success:
                    Status = ToastMessageStatus.Success;
                    break;
                case ResponseStatus.Failed:
                    Status = ToastMessageStatus.Error;
                    break;
                default:
                    Status = ToastMessageStatus.Info;
                    break;
            }
            Message = model.Message;
        }
        public ToastMessageStatus Status { get; set; }
        public string Message { get; set; }
    }
}