﻿namespace Pharmidex.Core.Models
{
    public class DropdownListItemModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public bool Selected { get; set; }
    }
}
