﻿using System.Net;

namespace Pharmidex.Core.Enums
{
    public enum UserRole
    {
        Admin = 1,
        Customer = 2,
    }

    public enum Platform
    {
        Android = 1,
        iOS = 2,
        Admin = 3,
        Web = 4
    }

    public enum ResponseStatus
    {
        Failed = 0,
        Success = HttpStatusCode.OK,//200
        AccountDeactivated = 3001,
        CompleteProfile = 3002,
        NotFound = HttpStatusCode.NotFound,//404
        Unauthorized = HttpStatusCode.Unauthorized,//401
        SubscriptionRequired = HttpStatusCode.PaymentRequired,//402
        SubscriptionExpired = HttpStatusCode.PreconditionFailed,//412
        UpgradeRequired = HttpStatusCode.UpgradeRequired,//426
        InternalServerError = HttpStatusCode.InternalServerError,//500
        ServiceUnavailable = HttpStatusCode.ServiceUnavailable,//503
        NoContent = HttpStatusCode.NoContent,//204
    }


    public enum EmailTemplate
    {
        Default,
        ForgetPassword
    }
    public enum StudyStatus
    {
        InProgress = 0,
        AwaitingInvoicing = 1,
        StudyInvoiced = 2,
        Archived = 3,
    }
}