﻿using Microsoft.AspNet.Identity.EntityFramework;
using Pharmidex.Core.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PharmidexEntity
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }
        public DateTime CreatedUTC { get; set; } = Utilities.GetSystemDateTimeUTC();
        public DateTime? UpdatedUTC { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<ApplicationUserLogin> ApplicationUserLogin { get; set; }
    }
}