﻿using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace PharmidexEntity.Base
{
    public abstract class BaseEntityId : IBaseEntityId
    {
        [Key]
        public int Id { get; set; }
    }

    public abstract class BaseEntity : IBaseEntity
    {
        public BaseEntity()
        {
            CreatedUTC = Utilities.GetSystemDateTimeUTC();
        }

        [Key]
        public int Id { get; set; }
        public DateTime CreatedUTC { get; set; }
    }
}