﻿using PharmidexEntity.Base;

namespace Pharmidex.Entity
{
    public class StudyType : BaseEntity
    {
        public string Title { get; set; }
    }
}
