﻿using PharmidexEntity.Base;

namespace Pharmidex.Entity
{
    public class Department : BaseEntity
    {
        public string Title { get; set; }
    }
}
