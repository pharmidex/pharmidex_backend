﻿using Pharmidex.Core.Enums;
using PharmidexEntity;
using PharmidexEntity.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pharmidex.Entity
{
    public class Studies : BaseEntity
    {
        [Required]
        [MaxLength(128)]
        public string UId { get; set; }
        [ForeignKey("UId")]
        public virtual ApplicationUser User { get; set; }

        [MaxLength(128)]
        public string CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual ApplicationUser Customer { get; set; }

        [Required]
        public string ProposalNo { get; set; }

        [Required]
        public DateTime ProposalDate { get; set; }

        public virtual ICollection<StudyDetails> StudyDetails { get; set; }
    }

    public class StudyDetails : BaseEntityId
    {
        [Required]
        public int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }

        [Required]
        public int StudyTypeId { get; set; }
        [ForeignKey("StudyTypeId")]
        public virtual StudyType StudyType { get; set; }

        [Required]
        public int DirectorId { get; set; }
        [ForeignKey("DirectorId")]
        public virtual Director Director { get; set; }

        public string StudyNo { get; set; }

        public string Compounds { get; set; }
        public DateTime CompoundsRecDate { get; set; }
        public DateTime VIVOStartDate { get; set; }
        public DateTime VIVOEndDate { get; set; }
        public DateTime SamplesReceivedDate { get; set; }
        public DateTime BAStartDate { get; set; }
        public DateTime BAEndDate { get; set; }
        public DateTime CompletionDate { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime SamplesShippedDate { get; set; }
        public string CompletionAuthorisedBy { get; set; }
        public string Notes { get; set; }
        public StudyStatus StudyStatus { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }

        [Required]
        public DateTime DateArchived { get; set; }

        public string SamplesDescription { get; set; }
    }
}
