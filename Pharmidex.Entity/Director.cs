﻿using PharmidexEntity.Base;

namespace Pharmidex.Entity
{
    public class Director:BaseEntityId
    {
        public string Title { get; set; }
    }
}
