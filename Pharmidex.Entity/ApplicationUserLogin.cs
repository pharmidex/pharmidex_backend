﻿using Pharmidex.Core.Enums;
using Pharmidex.Core.Helpers;
using PharmidexEntity.Base;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PharmidexEntity
{
    public class ApplicationUserLogin : BaseEntityId
    {
        [Required]
        [MaxLength(128)]
        public string UId { get; set; }

        [ForeignKey("UId")]
        public virtual ApplicationUser User { get; set; }

        [EnumDataType(typeof(Platform))]
        public Platform Platform { get; set; }

        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string IP { get; set; }
        public DateTime LoginUTC { get; set; } = Utilities.GetSystemDateTimeUTC();
        public DateTime? LogoutUTC { get; set; }
    }
}