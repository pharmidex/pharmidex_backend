﻿using PharmidexEntity.Base;
using System.ComponentModel.DataAnnotations;

namespace PharmidexEntity
{
    public class ApplicationSettings : BaseEntityId
    {
        public bool Maintenance { get; set; }
        public bool ForceUpdateiOS { get; set; }
        public bool ForceUpdateAndroid { get; set; }

        [Required(ErrorMessage = "Enter iOS version!")]
        public decimal iOSVersion { get; set; }

        [Required(ErrorMessage = "Enter Android version!")]
        public decimal AndroidVersion { get; set; }
    }
}