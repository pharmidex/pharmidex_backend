﻿using System.Web;
using System.Web.Optimization;

namespace Pharmidex.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region sign in

            bundles.Add(new StyleBundle("~/css/signin").Include(
                    "~/assets/libs/bootstrap/bootstrap.css",
                    "~/assets/css/pace.css",
                    "~/assets/css/bootstrap-extended.css",
                    "~/assets/css/colors.css",
                    "~/assets/css/components.css",
                    "~/assets/core/vertical-menu/vertical-menu.css",
                    "~/assets/core/palette-gradient/palette-gradient.css",
                    "~/assets/pages/signin/signin.css",
                    "~/assets/css/custom.css"
                    ));

            bundles.Add(new ScriptBundle("~/jQuery/signin").Include(
                 "~/assets/js/vendors.min.js",
                "~/assets/libs/bootstrap/bootstrap.min.js",
                "~/assets/js/app-menu.js",
                "~/assets/js/app.js",
                "~/assets/libs/validation/jquery.unobtrusive-ajax.min.js",
                "~/assets/libs/validation/jquery.validate.min.js",
                "~/assets/libs/validation/jquery.validate.unobtrusive.min.js"
                ));

            #endregion sign in

            #region forgotpassword

            bundles.Add(new StyleBundle("~/css/forgotpassword").Include(
                    "~/assets/libs/bootstrap/bootstrap.css",
                    "~/assets/css/pace.css",
                    "~/assets/css/bootstrap-extended.css",
                    "~/assets/css/colors.css",
                    "~/assets/css/components.css",
                    "~/assets/core/vertical-menu/vertical-menu.css",
                    "~/assets/core/palette-gradient/palette-gradient.css",
                     "~/assets/css/custom.css"
                    ));

            bundles.Add(new ScriptBundle("~/jquery/forgotpassword").Include(
                "~/assets/js/vendors.min.js",
                "~/assets/libs/bootstrap/bootstrap.min.js",
                "~/assets/js/app-menu.js",
                "~/assets/js/app.js",
                "~/assets/js/customizer.js",
                "~/assets/libs/validation/jquery.unobtrusive-ajax.min.js",
                "~/assets/libs/validation/jquery.validate.min.js",
                "~/assets/libs/validation/jquery.validate.unobtrusive.min.js"
                ));

            #endregion forgotpassword

            #region error

            bundles.Add(new StyleBundle("~/css/error").Include(
                    "~/assets/libs/bootstrap/bootstrap.css",
                    "~/assets/css/pace.css",
                    "~/assets/css/bootstrap-extended.css",
                    "~/assets/css/colors.css",
                    "~/assets/css/components.css",
                    "~/assets/core/vertical-menu/vertical-menu.css",
                    "~/assets/core/palette-gradient/palette-gradient.css",
                     "~/assets/css/custom.css",
                     "~/assets/pages/error/error.css"
                    ));

            bundles.Add(new ScriptBundle("~/jquery/error").Include(
                "~/assets/js/vendors.min.js",
                "~/assets/libs/bootstrap/bootstrap.min.js",
                "~/assets/js/app-menu.js"
                ));

            #endregion error

            #region ResetPassword

            bundles.Add(new StyleBundle("~/css/resetpassword").Include(
                    "~/assets/libs/bootstrap/bootstrap.css",
                    "~/assets/css/pace.css",
                    "~/assets/css/bootstrap-extended.css",
                    "~/assets/css/colors.css",
                    "~/assets/css/components.css",
                    "~/assets/core/vertical-menu/vertical-menu.css",
                    "~/assets/core/palette-gradient/palette-gradient.css",
                    "~/assets/pages/signin/signin.css",
                    "~/assets/css/custom.css"
                    ));

            bundles.Add(new ScriptBundle("~/jquery/resetpassword").Include(
                "~/assets/js/vendors.min.js",
                "~/assets/libs/bootstrap/bootstrap.min.js",
                "~/assets/js/app-menu.js",
                "~/assets/js/app.js",
                "~/assets/js/customizer.js",
                "~/assets/libs/validation/jquery.unobtrusive-ajax.min.js",
                "~/assets/libs/validation/jquery.validate.min.js",
                "~/assets/libs/validation/jquery.validate.unobtrusive.min.js"
                ));

            #endregion ResetPassword

            #region layout

            bundles.Add(new StyleBundle("~/css/layout").Include(
                "~/assets/libs/bootstrap/bootstrap.css",
                "~/assets/css/pace.css",
                "~/assets/css/bootstrap-extended.css",
                "~/assets/css/colors.css",
                "~/assets/css/components.css",
                "~/assets/core/vertical-menu/vertical-menu.css",
                "~/assets/core/palette-gradient/palette-gradient.css",
                "~/assets/css/custom.css"
                ));

            bundles.Add(new ScriptBundle("~/jquery/layout").Include(
                 "~/assets/js/vendors.min.js",
                "~/assets/libs/bootstrap/bootstrap.min.js",
                "~/assets/libs/loader/loadingoverlay.min.js",
                "~/assets/js/app-menu.js",
                "~/assets/js/app.js",
                "~/assets/js/customizer.js",
                "~/assets/js/custom.js"
                ));

            #endregion layout

            #region Summer note
            bundles.Add(new ScriptBundle("~/jQuery/summernote").Include(
                "~/assets/libs/Summernote/summernote.js",
                "~/assets/libs/Summernote/summernote.min.js"
                ));

            bundles.Add(new StyleBundle("~/css/summernote").Include(
              "~/assets/libs/Summernote/summernote.css"
              ));
            #endregion

            #region add update

            bundles.Add(new StyleBundle("~/css/addupdate").Include(
                "~/assets/libs/select2/select2.min.css"
                ));

            bundles.Add(new ScriptBundle("~/jquery/addupdate").Include(
                "~/assets/libs/validation/jquery.unobtrusive-ajax.min.js",
                "~/assets/libs/validation/jquery.validate.min.js",
                "~/assets/libs/validation/jquery.validate.unobtrusive.min.js",
                 "~/assets/libs/select2/select2.min.js"
                ));

            bundles.Add(new ScriptBundle("~/jquery/validate").Include(
                "~/assets/libs/validation/jquery.unobtrusive-ajax.min.js",
                "~/assets/libs/validation/jquery.validate.min.js",
                "~/assets/libs/validation/jquery.validate.unobtrusive.min.js"
                ));

            #endregion add update

            #region view grid

            bundles.Add(new StyleBundle("~/css/viewgrid").Include(
                "~/assets/libs/datatable/datatables.min.css"
                ));

            bundles.Add(new ScriptBundle("~/jquery/viewgrid").Include(
                "~/assets/libs/datatable/datatables.min.js"
                ));
            #endregion view grid

            #region Icheck

            bundles.Add(new StyleBundle("~/css/Icheck").Include(
                "~/assets/libs/iCheck/blue.css"
                ));

            bundles.Add(new ScriptBundle("~/jquery/Icheck").Include(
                    "~/assets/libs/iCheck/icheck.js"
                ));

            #endregion Icheck

            #region Bootstrap toggle
            bundles.Add(new StyleBundle("~/css/togglebutton").Include(
                "~/assets/libs/bootstrap-toggle/bootstrap4-toggle.min.css"));

            bundles.Add(new StyleBundle("~/jquery/togglebutton").Include(
               "~/assets/libs/bootstrap-toggle/bootstrap4-toggle.min.js"));
            #endregion

            #region Select2
            bundles.Add(new StyleBundle("~/css/select2").Include(
              "~/assets/libs/select2/select2.min.css"
             ));

            bundles.Add(new ScriptBundle("~/jquery/select2").Include(
               "~/assets/libs/select2/select2.min.js"
              ));
            #endregion

            #region repeater
            bundles.Add(new ScriptBundle("~/jQuery/repeater").Include(
              "~/assets/libs/repater/repeater.min.js"
            ));
            #endregion

            #region Update imageuploader
            bundles.Add(new StyleBundle("~/css/imageuploader").Include(
                "~/assets/libs/imageuploader/css/cropper.css",
                "~/assets/libs/imageuploader/css/imageuploader.css"
                ));

            bundles.Add(new ScriptBundle("~/jQuery/imageuploader").Include(
                "~/assets/libs/imageuploader/js/cropper.js",
                "~/assets/libs/imageuploader/js/imageuploader.js"
                ));
            #endregion

            #region Date time picker
            bundles.Add(new StyleBundle("~/css/Datetimepicker").Include(
                "~/assets/libs/datetime/bootstrap-datetimepicker.css"
                ));

            bundles.Add(new ScriptBundle("~/jQuery/Datetimepicker").Include(
                "~/assets/libs/datetime/bootstrap-datetimepicker.min.js"
                ));
            #endregion

            #region touchspin
            bundles.Add(new StyleBundle("~/css/touchspin").Include(
                   "~/Assets/libs/touchspin/touchspin.css"
                ));

            bundles.Add(new ScriptBundle("~/jQuery/touchspin").Include(
               "~/Assets/libs/touchspin/touchspin.js"
            ));
            #endregion

            bundles.Add(new ScriptBundle("~/js/GoogleMap").Include(
                    "~/assets/libs/GoogleMap/mapmodal.js"
                    ));

            bundles.Add(new StyleBundle("~/css/dragula").Include(
               "~/assets/libs/dragula/dragula.min.css"
           ));

            bundles.Add(new ScriptBundle("~/jQuery/dragula").Include(
             "~/assets/libs/dragula/dragula.min.js"
          ));

            BundleTable.EnableOptimizations = false;

        }
    }
}
