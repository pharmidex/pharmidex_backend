﻿using AutoMapper;
using AutoMapper.Configuration;

namespace Pharmidex.Admin
{
    public class MapperConfig
    {
        public static void Configure()
        {
            MapperConfigurationExpression config = new MapperConfigurationExpression();

            Mapper.Initialize(config);
        }
    }
}