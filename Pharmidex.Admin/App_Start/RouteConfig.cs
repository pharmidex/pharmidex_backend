﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Pharmidex.Admin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;

            routes.MapRoute(
                 name: "Signin",
                 url: "Signin",
                 defaults: new { controller = "Account", action = "Signin" }
             );

            routes.MapRoute(
                 name: "appsettings",
                 url: "appsettings",
                 defaults: new { controller = "ApplicationSettings", action = "Index" }
             );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}