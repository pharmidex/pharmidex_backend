﻿function toggleActivate(control, title) {
    var isActivated = $(control).hasClass('active');
    var status = (isActivated ? "Deactivated" : "Activated")
    var title = "Selected user will be " + status + "!!";
    control.children('i').removeClass(isActivated ? 'fa-check' : 'fa-times').addClass('fa-spinner spinner');
    swal({
        title: title,
        text: "Are you sure ?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel it!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes!",
                value: true,
                visible: true,
                className: "btn-success",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            $.post($(control).data('url'), { 'UId': $(control).data('id') }, function (response) {
                if (response.Status == 0) {
                    control.children('i').removeClass('fa-spinner spinner').addClass(isActivated ? 'fa-check' : 'fa-times');
                    swal("Error", response.Message, "error");
                }
                else {
                    control.children('i').removeClass('fa-spinner spinner').addClass(isActivated ? 'fa-times' : 'fa-check');
                    swal("Completed!", response.Message, "success");
                    /*$(control).find('span').text(status)*/
                    if (isActivated) {
                        $(control).removeClass('active btn-green');
                        $(control).addClass('btn-red');
                    }
                    else {
                        $(control).addClass('active btn-red');
                        $(control).addClass('btn-green');
                    }
                }
            });
        } else {
            control.children('i').removeClass('fa-spinner spinner').addClass(isActivated ? 'fa-check' : 'fa-times');
            swal("Cancelled", "Your record is safe :)", "info");
        }
    });
}

function deleteuser(control, title) {
    title = "Selected user will be deleted!!";
    control.children('i').removeClass('fa-trash').addClass('fa-spinner spinner');
    swal({
        title: title,
        text: "Are you sure ?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel it!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes!",
                value: true,
                visible: true,
                className: "btn-success",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            $.post($(control).data('url'), { 'UId': $(control).data('schoolid'), 'TeacherId': $(control).data('id') }, function (response) {
                if (response.Status == 0) {
                    swal("Error", response.Message, "error");
                }
                else {
                    swal("Completed!", response.Message, "success");
                    $(control).removeClass('btnDelete').text('Removed');
                    $(control).parent().find('.toggleActivate').remove();
                    $(control).parent().find('.btnEdit').remove();
                    $(control).find("i").remove();
                }
                control.children('i').removeClass('fa-spinner spinner').addClass('fa-trash');
            });
        } else {
            control.children('i').removeClass('fa-spinner spinner').addClass('fa-trash');
            swal("Cancelled", "Your record is safe :)", "info");
        }
    });
}