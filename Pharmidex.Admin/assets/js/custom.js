﻿$(function () {
    $('form').submit(function () {
        if ($(this).valid()) {
            $(':submit').text("Please wait")
            $(':submit').attr('disabled', 'disabled');
        }
    });
});

$(document).on('click', '.toggleActivate', function () {
    ActiveToggle($(this));
});

ActiveMenu = (liId) => {
    $("#li" + liId).addClass("active");
}

DeleteRecord = (control, title) => {
    if (!title) {
        title = "Are you sure do you want to delete this record?"
    }

    Swal.fire({
        title: title,
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: "No, cancel!",
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        }
    }).then((result) => {
        if (result.isConfirmed) {
            var Url = $(control).attr('data-url');
            $.post(Url, { 'Id': $(control).attr('data-id') }, function (response) {
                if (response.Status == 0) {
                    control.children('i').removeClass('fa-spinner spinner').addClass('fa-trash');
                    Swal.fire("Error", response.Message, "error");
                }
                else {
                    control.parent().parent().hide(200);
                    $('#datatable').DataTable().ajax.reload();
                    Swal.fire("Success!", response.Message, "success");
                }
            });
        }
        else {
            control.children('i').addClass('fa-trash').removeClass('fa-spinner spinner');
        }
    })
}

ActiveToggle = (control, title) => {
    var isActivated = $(control).hasClass('active');
    var status = (isActivated ? "Deactivate" : "Activate")
    var title = "Are you sure you want to " + status + "?";
    control.children('i').removeClass(isActivated ? 'fa-check' : 'fa-times').addClass('fa-spinner spinner');

    Swal.fire({
        title: title,
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, do it!',
        cancelButtonText: "No, cancel!",
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        }
    }).then((result) => {
        if (result.isConfirmed) {
            $.post($(control).data('url'), { 'Id': $(control).data('id') }, function (response) {
                if (response.Status == 0) {
                    control.children('i').removeClass('fa-spinner spinner').addClass(isActivated ? 'fa-check' : 'fa-times');
                    Swal.fire("Error", response.Message, 'error')
                }
                else {
                    if (isActivated) {
                        $(control).removeClass('active btn-green').addClass('btn-red');
                        control.html(" <i class= 'fa fa-times' aria-hidden='true'></i> Deactivated");
                    }
                    else {
                        $(control).addClass('active btn-green').removeClass('btn-red');
                        control.html(" <i class= 'fa fa-check' aria-hidden='true'></i> Activated");
                    }
                    Swal.fire(status, response.Message, 'success')
                }
            });
        } else {
            control.children('i').removeClass('fa-spinner spinner').addClass(isActivated ? 'fa-check' : 'fa-times');
        }
    })
}

StartBlockPage = () => {
    $.LoadingOverlaySetup({
        background: "rgba(0, 0, 0, 0.3)",
        imageAnimation: "1.5s fadein",
        imageColor: "#007fcc",
    });
    $.LoadingOverlay("show");
}

CloseBlockPage = () => {
    $.LoadingOverlay("hide");
}

StartLoaderById = (element) => {
    $("#" + element).LoadingOverlay("show");
}

StopLoaderById = (element) => {
    $("#" + element).LoadingOverlay("hide");
}

StartLoaderByClass = (element) => {
    $("." + element).LoadingOverlay("show");
}

StopLoaderBYClass = (element) => {
    $("." + element).LoadingOverlay("hide");
}

