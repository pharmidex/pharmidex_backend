﻿function RemoveFromDiscover(control, title) {
    title = "Selected artist will be removed from discover!!";
    control.children('i').removeClass('fa-check').addClass('fa-spinner spinner');
    swal({
        title: title,
        text: "Are you sure ?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel it!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes!",
                value: true,
                visible: true,
                className: "btn-success",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            $.post($(control).data('url'), { 'UId': $(control).data('id') }, function (response) {
                if (response.Status == 0) {
                    control.children('i').removeClass('fa-spinner spinner').addClass('fa-check');
                    swal("Error", response.Message, "error");
                }
                else {
                    control.children('i').removeClass('fa-spinner spinner').addClass('fa-times');
                    swal("Completed!", response.Message, "success");
                    $(control).find('span').text('Removed');
                    $(control).removeClass('active btn-green');
                    $(control).addClass('btn-red');
                }
            });
        } else {
            control.children('i').removeClass('fa-spinner spinner').addClass('fa-check');
            swal("Cancelled", "Your record is safe :)", "info");
        }
    });
}