﻿function SaveCropedImage(saveFilePath, hdnImageName, postUrl) {
    console.log('postUrl' + postUrl)
    StartBlockPage();
    var $image = $("#cropimage_" + hdnImageName);
    var result = $image.cropper("getCroppedCanvas");
    var imgData = result.toDataURL('image/png');

    var extension = $("#fileImage_" + hdnImageName).val().split('.').pop();
    var prevImageName = $("#prev_" + hdnImageName).val();

    var imageExtensions = ["png", "jpg", "jpeg"];
    var index = jQuery.inArray(extension, imageExtensions);
    if (index == -1) {
        extension = "jpg";
    }
    var model = {
        ImgBase64String: imgData,
        SaveFilePath: saveFilePath,
        Extension: extension,
        PreviousImageName: prevImageName
    };

    $.post(postUrl, model).done(function (resultResponse) {
        if (resultResponse.Status == 0) {
            toastr.fire({ icon: "error", title: resultResponse.Message });
        }
        else {
            toastr.fire({ icon: "success", title: resultResponse.Message });
        }

        $('#preview_' + hdnImageName).attr('src', imgData);
        $("#" + hdnImageName).val(resultResponse.Result);
        $("#prev_" + hdnImageName).val(resultResponse.Result);
        $("#Modal_" + hdnImageName).modal('hide');
        CloseBlockPage();
    }).fail(function (err) {
        $("#Modal_" + hdnImageName).modal('hide');
        toastr.fire({ icon: "error", title: err.statusText });
        CloseBlockPage();
    });
}
