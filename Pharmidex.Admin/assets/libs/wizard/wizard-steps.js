/*=========================================================================================
    File Name: wizard-steps.js
    Description: wizard steps page specific js
    ----------------------------------------------------------------------------------------
    Item Name: Robust - Responsive Admin Theme
    Version: 1.2
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Validate steps wizard

var form = $("#steps-wizard");

form.steps({
    startIndex: 0,
    //enableAllSteps: 1,
    headerTag: "h6",
    bodyTag: "fieldset",
    transitionEffect: "fade",
    transitionEffectSpeed: 400,
    //showFinishButtonAlways: 1,
    saveState: false,
    titleTemplate: '<span class="step">#index#</span> #title#',
    enableFinishButton: false,
    onStepChanging: function (event, currentIndex, newIndex) {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex) {
            return true;
        }

        form.validate().settings.ignore = ":disabled,:hidden";

        if (currentIndex < newIndex) {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }

        return form.valid();
    },
    onStepChanged: function (event, currentIndex, priorIndex) {
    },
    onFinishing: function (event, currentIndex) {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex) {
        form.submit();
    }
});