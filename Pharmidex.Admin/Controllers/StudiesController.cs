﻿using AmarnaEnergy.IServices;
using Microsoft.AspNet.Identity;
using Pharmidex.Core.Enums;
using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Pharmidex.Admin.Controllers
{
    public class StudiesController : ApplicationBaseController
    {
        private readonly IStudiesService _service;
        public StudiesController(IStudiesService service)
        {
            _service = service;
        }

        // GET: Studies
        public async Task<ActionResult> Index() { await BindStudiesData(); return View(); }

        [HttpPost]
        public async Task<JsonResult> Index(DataTableAjaxPostModel model) => Json(await _service.GetFilteringList(model));

        public async Task<ActionResult> Add()
        {
            await BindStudiesData();
            return View("AddUpdate", new ManageStudiesModel());
        }

        [HttpPost]
        public async Task<ActionResult> Add(ManageStudiesModel model)
        {
            if (ModelState.IsValid)
            {
                model.UId = User.Identity.GetUserId();
                ResponseModel mResult = await _service.AddUpdate(model);
                OnBindToastMessage(mResult);
                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToAction("Index");
            }
            await BindStudiesData();
            return View("AddUpdate", model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            ResponseModel<ManageStudiesModel> mResult = await _service.GetDetailsForEdit(id);
            if (mResult != null && mResult.Result != null && mResult.Status == ResponseStatus.Success)
            {
                await BindStudiesData();
                return View("AddUpdate", mResult.Result);
            }
            OnBindToastMessage(mResult);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(ManageStudiesModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel mResult = await _service.AddUpdate(model);
                OnBindToastMessage(mResult);
                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToAction("Index");
            }
            return View("AddUpdate", model);
        }

        [HttpPost]
        public async Task<JsonResult> Delete(int id) => Json(await _service.DeleteAsync(new IBaseIdModel() { Id = id }));

        public async Task<PartialViewResult> AddNewStudyDetails()
        {
            await BindStudiesData();
            return PartialView("_StudyDetails", new StudyDetailsManageModel() { StudyNo = "PSN19-" + Utilities.GetRandomAlphaNumeric(3) });
        }

    }
}