﻿using Pharmidex.Core.Enums;
using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using PharmidexServices.IServices;
using Microsoft.AspNet.Identity;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Pharmidex.Admin.Controllers
{
    public class AccountController : ApplicationBaseController
    {
        private readonly IAccountService _service;
        public AccountController(IAccountService service)
        {
            _service = service;
        }

        [AllowAnonymous]
        public ActionResult Signin()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Signin(SigninRequestModel model)
        {
            model.UserRole = UserRole.Admin;
            model.Platform = Platform.Admin;
            ResponseModel mResult = await _service.Signin(model);
            OnBindToastMessage(mResult);
            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            return View(model);
        }

        #region Signout
        [HttpGet]
        public async Task<ActionResult> SignOut()
        {
            ResponseModel<object> mResult = await _service.SignOut(new SignoutRequestModel()
            {
                IsSignout = true,
                Platform = Platform.Admin,
                UId = User.Identity.GetUserId()
            });
            OnBindToastMessage(mResult);
            return RedirectToRoutePermanent("Signin");
        }
        #endregion

        #region ChangePassword
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePasswordRequestModel model)
        {
            model.UId = User.Identity.GetUserId();
            ResponseModel<object> mResult = await _service.ChangePasswordAsync(model);
            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassdddword(ChangePasswordRequestModel model)
        {
            model.UId = User.Identity.GetUserId();
            ResponseModel<object> mResult = await _service.ChangePasswordAsync(model);
            OnBindToastMessage(mResult);

            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            return View(model);
        }
        #endregion

        #region ForgotPassword

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordRequestModelAdmin model)
        {
            model.Platform = Platform.Admin;
            ResponseModel<object> mResult = await _service.ForgotPasswordAdmin(model);
            OnBindToastMessage(mResult);
            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToRoutePermanent("Signin");
            }
            return View(model);
        }

        #endregion ForgotPassword


        [AllowAnonymous]
        public ActionResult EnterCode()
        {
            return View();
        }

        #region ResetPassword

        public ActionResult ResetPasswordConfirmed()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string passwordresettoken, string email)
        {
            string decodeToken = HttpUtility.UrlDecode(passwordresettoken, Encoding.UTF8);
            IResetPasswordRequestModel model = new ResetPasswordRequestModel()
            {
                PasswordResetToken = decodeToken,
                Email = email,
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordRequestModel model)
        {
            ResponseModel<object> mResult = await _service.ResetPassword(model);
            OnBindToastMessage(mResult);
            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToRoutePermanent("Signin");
            }
            return View(model);
        }

        #endregion ResetPassword
    }
}