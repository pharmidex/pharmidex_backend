﻿using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models.Base;
using PharmidexServices.Services;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Pharmidex.Admin.Controllers
{
    public class ApplicationBaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!ModelState.IsValid)
            {
                string errorMessage = ModelState.Values.SelectMany(x => x.Errors).FirstOrDefault().ErrorMessage;

                OnBindToastMessage(new ResponseModel()
                {
                    Message = errorMessage
                });
            }
            base.OnActionExecuting(context);
            OnInitBreadcrumb();
        }

        public void OnBindToastMessage(IResponseModel model)
        {
            TempData["ToastMessageModel"] = new ToastMessageModel(model);
        }

        public void OnInitBreadcrumb()
        {
            string actionName = ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.Action = actionName.ToFirstCharUpper();
        }

        public async Task BindStudiesData()
        {
            using (NonDependencyService _service = new NonDependencyService())
            {
                ViewData["DirectorList"] = await _service.GetDirector();
                ViewData["DepartmentList"] = await _service.GetDepartment();
                ViewData["StudyTypeList"] = await _service.GetStudyType();
                ViewData["CustomersList"] = await _service.GetCustomers();
            }

        }
    }
}