﻿using Pharmidex.Core.Models.Base;
using PharmidexEntity;
using PharmidexServices.IServices;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Pharmidex.Admin.Controllers
{
    public class ApplicationSettingsController : ApplicationBaseController
    {
        private readonly IApplicationSettingService _service;

        public ApplicationSettingsController(IApplicationSettingService service)
        {
            _service = service;
        }

        public async Task<ActionResult> Index()
        {
            ApplicationSettings settings = await _service.GetApplicationSettingAsync();
            return View(settings);
        }

        [HttpPost]
        public async Task<ActionResult> Index(ApplicationSettings model)
        {
            ResponseModel mResult = await _service.UpdateApplicationSettingsAsync(model);
            OnBindToastMessage(mResult);
            return View(model);
        }
    }
}