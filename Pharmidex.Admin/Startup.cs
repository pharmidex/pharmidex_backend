﻿using Pharmidex.Admin;
using Pharmidex.Core.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartup("OWIN", typeof(Startup))]
namespace Pharmidex.Admin
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
            app.UseCors(CorsOptions.AllowAll);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to
            // store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                CookieHttpOnly = true,
                CookieName = "Admin_" + GlobalConfig.ProjectName,
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Signin"),
                LogoutPath = new PathString("/Account/Signout"),
            });
        }
    }
}