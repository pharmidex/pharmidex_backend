﻿using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models.Base;
using PharmidexEntity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PharmidexServices.IServices
{
    public interface IApplicationSettingService
    {
        Task<ApplicationSettings> GetApplicationSettingAsync();
        Task<ResponseModel> UpdateApplicationSettingsAsync(ApplicationSettings model);        
    }
}