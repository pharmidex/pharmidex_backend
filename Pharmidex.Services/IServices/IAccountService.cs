﻿using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using PharmidexEntity;
using System;
using System.Threading.Tasks;

namespace PharmidexServices.IServices
{
    public interface IAccountService : IDisposable
    {
        Task<AuthenticateResultModel> UserAuthenticate(SigninRequestModel model);
        Task<ResponseModel<SigninResultModel>> Signin(SigninRequestModel model);
        void Signout();
        Task<ResponseModel<SigninResultModel>> SignUp(SignupModel model);
        Task<ApplicationUser> FindByIdAsync(string UserId);
        Task<ResponseModel<object>> ResetPassword(IResetPasswordRequestModel model);
        Task<ResponseModel<object>> ForgotPasswordAdmin(ForgotPasswordRequestModelAdmin model);        
        Task<ResponseModel<object>> ChangePasswordAsync(IChangePasswordRequestModel model);
        Task<ResponseModel<object>> SignOut(IUserSinginLogModel model);
        Task CreateIdentityAsync(string userId);
    }
}