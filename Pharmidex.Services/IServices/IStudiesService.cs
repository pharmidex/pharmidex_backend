﻿using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using System.Threading.Tasks;

namespace AmarnaEnergy.IServices
{
    public interface IStudiesService
    {
        Task<ResponseModel> AddUpdate(ManageStudiesModel model);
        Task<DataTableAjaxResultModel> GetFilteringList(DataTableAjaxPostModel model);
        Task<ResponseModel<ManageStudiesModel>> GetDetailsForEdit(int Id);
        Task<ResponseModel> DeleteAsync(IBaseEntityId model);
    }
}