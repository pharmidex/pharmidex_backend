﻿using Pharmidex.Core.Models;
using PharmidexData.Repository;
using PharmidexEntity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PharmidexServices.Services
{
    public class NonDependencyService : IDisposable
    {
        private NonDependencyRepository _repo;

        public NonDependencyService()
        {
            _repo = new NonDependencyRepository();
        }

        public async Task<ApplicationSettings> GetApplicationSettingAsync() => await _repo.GetApplicationSettingAsync();

        public async Task<UserAuthorizeFilterModel> FindByIdAsync(string UserId) => await _repo.FindByIdAsync(UserId);

        public async Task<List<DropdownListItemModel>> GetDepartment() => await _repo.GetDepartment();

        public async Task<List<DropdownListItemModel>> GetStudyType() => await _repo.GetStudyType();

        public async Task<List<DropdownListItemModel>> GetDirector() => await _repo.GetDirector();
        public async Task<List<DropdownListItemModel>> GetCustomers() => await _repo.GetCustomers();

        public void Dispose() => _repo.Dispose();
    }
}