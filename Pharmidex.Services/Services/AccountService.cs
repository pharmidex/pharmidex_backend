﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Pharmidex.Core.Enums;
using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using Pharmidex.Core.Resources;
using PharmidexData.Identities;
using PharmidexData.IRepository;
using PharmidexEntity;
using PharmidexServices.IServices;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PharmidexServices.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _repo;

        public AccountService(IAccountRepository repo)
        {
            _repo = repo;
        }

        public async Task<AuthenticateResultModel> UserAuthenticate(SigninRequestModel model)
        {
            AuthenticateResultModel result = null;

            ApplicationUser user = await _repo.UserAuthenticate(model);
            if (user != null)
            {
                result = new AuthenticateResultModel()
                {
                    UId = user.Id,
                    Name = user.Name
                };
            }
            return result;
        }

        #region Signin
        public async Task<ResponseModel<SigninResultModel>> Signin(SigninRequestModel model)
        {
            ResponseModel<SigninResultModel> mResult = new ResponseModel<SigninResultModel>()
            {
                Status = ResponseStatus.Failed
            };

            try
            {
                ApplicationUser user = await _repo.FindByEmailAsync(model);
                if (user != null)
                {
                    if (user.IsActive)
                    {
                        bool validPassword = await _repo.CheckPasswordAsync(user, model.Password);
                        if (validPassword)
                        {
                            if (model.Platform == Platform.Web)
                            {
                                bool IsValidRole = await _repo.IsInRoleAsync(new RoleRequestModel { UId = user.Id, UserRole = UserRole.Admin });
                                if (IsValidRole == false)
                                {
                                    mResult.Message = ResponseMessages.InvalidUserRole;
                                    return mResult;
                                }
                            }
                            mResult = await Signin(user, model, model.Password);
                        }
                        else
                        {
                            mResult.Message = ResponseMessages.InvalidPassword;
                        }
                    }
                    else
                    {
                        mResult.Message = ResponseMessages.AccountDeactivated;
                    }
                }
                else
                {
                    mResult.Message = ResponseMessages.EmailNotMatchAnyAccount;
                }
            }
            catch (DbEntityValidationException err)
            {
                StringBuilder sb = new StringBuilder();
                foreach (DbEntityValidationResult eve in err.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        sb.Append($"Property: {ve.PropertyName}, Error: {ve.ErrorMessage} {Environment.NewLine}");
                    }
                }
                mResult.Message = sb.ToString();
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }

            return mResult;
        }
        #endregion

        public void Signout()
        {
            _repo.IdentitySignout();
        }

        public async Task<ResponseModel<SigninResultModel>> SignUp(SignupModel model)
        {
            ResponseModel<SigninResultModel> mResult = new ResponseModel<SigninResultModel>();

            try
            {
                ApplicationUser user = await _repo.FindByEmailAsync(model);
                if (user == null)
                {
                    user = new ApplicationUser()
                    {
                        UserName = model.Email,
                        Email = model.Email,
                        PhoneNumber = model.PhoneNumber,
                        Name = model.Name,
                        IsActive = true,
                    };
                    IdentityResult result = await _repo.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        result = await _repo.AddToRoleAsync(new RoleRequestModel() { UId = user.Id, UserRole = model.UserRole });
                        if (result.Succeeded)
                        {
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = ResponseMessages.RegistrationSuccess;

                            mResult.Result = new SigninResultModel
                            {
                                UId = user.Id
                            };

                            if (model.Platform == Platform.Android || model.Platform == Platform.iOS)
                            {
                                model.UId = user.Id;
                                await _repo.SaveSigninLog(model);

                                ResponseModel<BearerAccessTokenResultModel> tokenResult = await GetAccessToken(new SigninRequestModel() { Email = user.Email, Password = model.Password });

                                if (tokenResult != null)
                                {
                                    if (tokenResult.Status == ResponseStatus.Success)
                                    {
                                        mResult.Status = ResponseStatus.Success;
                                        mResult.Result.AccessToken = tokenResult.Result;
                                    }
                                    else
                                    {
                                        mResult.Status = ResponseStatus.Failed;
                                        mResult.Message = tokenResult.Message;
                                        return mResult;
                                    }
                                }
                                else
                                {
                                    mResult.Status = ResponseStatus.Failed;
                                    mResult.Message = ResponseMessages.RegistrationFailed;
                                    return mResult;
                                }
                            }
                            else
                            {
                                await _repo.CreateIdentityAsync(user, false, false);
                                mResult.Status = ResponseStatus.Success;
                                mResult.Message = ResponseMessages.RegistrationSuccess;
                            }
                        }
                        else
                        {
                            mResult.Message = string.Join("; ", result.Errors);
                        }
                    }
                    else
                    {
                        mResult.Message = string.Join("; ", result.Errors);
                    }
                }
                else
                {
                    mResult.Message = ResponseMessages.AlreadyRegistered;
                }
            }
            catch (DbEntityValidationException err)
            {
                StringBuilder sb = new StringBuilder();
                foreach (DbEntityValidationResult eve in err.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        sb.Append($"Property: {ve.PropertyName}, Error: {ve.ErrorMessage} {Environment.NewLine}");
                    }
                }
                mResult.Message = sb.ToString();
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }
            return mResult;
        }

        #region Private methods
        private async Task<ResponseModel<SigninResultModel>> Signin(ApplicationUser user, UserSinginLogModel model, string password)
        {
            ResponseModel<SigninResultModel> mResult = new ResponseModel<SigninResultModel>()
            {
                Result = new SigninResultModel()
            };

            try
            {
                bool hasRole = await _repo.IsInRoleAsync(new RoleRequestModel() { UId = user.Id, UserRole = model.UserRole });
                if (hasRole)
                {
                    model.UId = user.Id;
                    await _repo.SaveSigninLog(model);

                    mResult = new ResponseModel<SigninResultModel>()
                    {
                        Status = ResponseStatus.Success,
                        Message = ResponseMessages.SignedinSuccess,
                        Result = new SigninResultModel()
                        {
                            UId = user.Id
                        }
                    };
                    if (model.Platform == Platform.Android || model.Platform == Platform.iOS)
                    {
                        ResponseModel<BearerAccessTokenResultModel> tokenResult = new ResponseModel<BearerAccessTokenResultModel>()
                        {
                            Status = ResponseStatus.Success
                        };
                        tokenResult = await GetAccessToken(new SigninRequestModel() { Email = user.Email, Password = password });
                        if (tokenResult != null && tokenResult.Status == ResponseStatus.Success)
                        {
                            if (tokenResult.Status == ResponseStatus.Success)
                            {
                                mResult.Result.AccessToken = tokenResult.Result;
                            }
                            else
                            {
                                mResult.Status = ResponseStatus.Failed;
                                mResult.Message = tokenResult.Message;
                            }
                        }
                        else
                        {
                            mResult.Status = ResponseStatus.Failed;
                            mResult.Message = ResponseMessages.LoginFailed;
                        }
                    }
                    else
                    {
                        await _repo.CreateIdentityAsync(user, model.UserRole);
                    }
                }
                else
                {
                    //mResult.Message = ResponseMessages.InvalidRole;
                    mResult.Message = $"You are not registered as {model.UserRole.ToString()}, Please check your account type!";
                }
            }
            catch (DbEntityValidationException err)
            {
                StringBuilder sb = new StringBuilder();
                foreach (DbEntityValidationResult eve in err.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        sb.Append($"Property: {ve.PropertyName}, Error: {ve.ErrorMessage} {Environment.NewLine}");
                    }
                }
                mResult.Message = sb.ToString();
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }

            return mResult;
        }

        private async Task<ResponseModel<BearerAccessTokenResultModel>> GetAccessToken(SigninRequestModel model)
        {
            ResponseModel<BearerAccessTokenResultModel> mResult = new ResponseModel<BearerAccessTokenResultModel>();

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    List<KeyValuePair<string, string>> requestParams = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", model.Email),
                    new KeyValuePair<string, string>("password", model.Password),
                };

                    HttpRequest request = HttpContext.Current.Request;
                    string tokenServiceUrl = request.Url.GetLeftPart(UriPartial.Authority) + request.ApplicationPath + "/Token";
                    FormUrlEncodedContent requestParamsFormUrlEncoded = new FormUrlEncodedContent(requestParams);
                    HttpResponseMessage tokenServiceResponse = await client.PostAsync(tokenServiceUrl, requestParamsFormUrlEncoded);
                    string responseString = await tokenServiceResponse.Content.ReadAsStringAsync();
                    HttpStatusCode responseCode = tokenServiceResponse.StatusCode;
                    if (responseCode == HttpStatusCode.OK)
                    {
                        mResult.Result = JsonConvert.DeserializeObject<BearerAccessTokenResultModel>(responseString);
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "OK";
                    }
                    else
                    {
                        mResult.Message = tokenServiceResponse.RequestMessage.ToString();
                    }
                }
            }
            catch (DbEntityValidationException err)
            {
                StringBuilder sb = new StringBuilder();
                foreach (DbEntityValidationResult eve in err.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        sb.Append($"Property: {ve.PropertyName}, Error: {ve.ErrorMessage} {Environment.NewLine}");
                    }
                }
                mResult.Message = sb.ToString();
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }

            return mResult;
        }
        #endregion

        public async Task<ApplicationUser> FindByIdAsync(string UserId)
        {
            return await _repo.FindByIdAsync(UserId);
        }

        public async Task<ResponseModel<object>> ForgotPasswordAdmin(ForgotPasswordRequestModelAdmin model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Failed
            };

            ApplicationUser user = await _repo.FindByEmailAsync(model);
            if (user != null)
            {
                bool hasRole = await _repo.IsInRoleAsync(new RoleRequestModel() { UId = user.Id, UserRole = model.UserRole });
                if (hasRole)
                {
                    string PasswordResetToken = await _repo.GeneratePasswordResetTokenAsync(
                        new UserIdModel()
                        {
                            UId = user.Id
                        });

                    string EncodeToken = HttpUtility.UrlEncode(PasswordResetToken);

                    string ResetPasswordlink = string.Format("{0}ResetPassword?passwordresettoken={1}&email={2}", GlobalConfig.AdminBaseUrl, EncodeToken, HttpUtility.UrlEncode(model.Email));

                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>
                    {
                        { "{{name}}", $"{user.Name}" },
                        { "{{baseurl}}", GlobalConfig.BaseUrl },
                        { "{{resetlink}}", ResetPasswordlink }
                    };

                    IdentityMessage message = new IdentityMessage
                    {
                        Subject = $"{GlobalConfig.ProjectName}: Reset Password",
                        Body = await Utilities.GenerateEmailBodyAsync(EmailTemplate.ForgetPassword, dicPlaceholders),
                        Destination = user.Email
                    };
                    await new EmailService().SendAsync(message);
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = ResponseMessages.PasswordResetEmailSent;
                }
                else
                {
                    mResult.Message = ResponseMessages.InvalidAdminEmail;
                }
            }
            else
            {
                mResult.Message = ResponseMessages.NoUserRegisteredEmail;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ChangePasswordAsync(IChangePasswordRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser user = await FindByIdAsync(model.UId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Unauthorized;
                mResult.Message = "InvalidAccess";
            }
            else
            {
                IdentityResult result = await _repo.ChangePasswordAsync(model);
                if (result.Succeeded)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = ResponseMessages.PasswordChanged;
                }
                else
                {
                    mResult.Message = string.Join(",", result.Errors);
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ResetPassword(IResetPasswordRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser user = await _repo.FindByEmailAsync(model);

            if (user != null)
            {
                model.UId = user.Id;
                bool isSendEmail = !string.IsNullOrWhiteSpace(user.PasswordHash);
                IdentityResult result = await _repo.ResetPasswordAsync(model);
                if (result.Succeeded)
                {
                    if (isSendEmail)
                    {
                        StringBuilder mailContent = new StringBuilder();
                        mailContent.Append($"Hello {user.UserName},");
                        mailContent.Append(GlobalConfig.BreakRow);
                        mailContent.Append(GlobalConfig.BreakRow);
                        mailContent.Append($"Greetings from {GlobalConfig.ProjectName} as per your request, we have successfully changed your password.");
                        mailContent.Append(GlobalConfig.BreakRow + GlobalConfig.BreakRow);
                        mailContent.Append("Thanks");
                        mailContent.Append(GlobalConfig.BreakRow);
                        mailContent.Append($"{GlobalConfig.ProjectName} Team!");

                        IdentityMessage message = new IdentityMessage()
                        {
                            Body = mailContent.ToString(),
                            Destination = model.Email,
                            Subject = $"{GlobalConfig.ProjectName}: Password reset"
                        };
                        await new EmailService().SendAsync(message);
                    }
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Password reset successfully!";
                }
                else
                {
                    mResult.Message = string.Join("; ", result.Errors);
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SignOut(IUserSinginLogModel model)
        {
            await _repo.SaveSigninLog(model);
            if (model.Platform == Platform.Admin)
            {
                _repo.IdentitySignout();
            }
            return new ResponseModel<object>()
            {
                Status = ResponseStatus.Success,
                Message = ResponseMessages.SignedoutSuccess
            };
        }

        public async Task CreateIdentityAsync(string userId)
        {
            ApplicationUser user = await _repo.FindByIdAsync(userId);
            if (user != null)
            {
                _repo.IdentitySignout();
                await _repo.CreateIdentityAsync(user, UserRole.Admin);
            }
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}