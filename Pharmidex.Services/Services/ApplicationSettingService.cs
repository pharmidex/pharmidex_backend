﻿using Pharmidex.Core.Enums;
using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models.Base;
using Pharmidex.Core.Resources;
using PharmidexData.IRepository;
using PharmidexEntity;
using PharmidexServices.IServices;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Threading.Tasks;

namespace PharmidexServices.Services
{
    public class ApplicationSettingService : IApplicationSettingService
    {
        private readonly IApplicationSettingRepository _repo;

        public ApplicationSettingService(IApplicationSettingRepository repo)
        {
            _repo = repo;
        }        

        public async Task<ApplicationSettings> GetApplicationSettingAsync()
        {
            return await _repo.FindAll(null).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel> UpdateApplicationSettingsAsync(ApplicationSettings entity)
        {
            ResponseModel mResult = new ResponseModel();
            try
            {
                await _repo.Edit(entity);
                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.ApplicationSettingsUpdated;
            }
            catch (DbEntityValidationException err)
            {
                foreach (DbEntityValidationResult validationErrors in err.EntityValidationErrors)
                {
                    foreach (DbValidationError validationError in validationErrors.ValidationErrors)
                    {
                        if (!string.IsNullOrWhiteSpace(mResult.Message))
                        {
                            mResult.Message += Environment.NewLine;
                        }
                        mResult.Message += $"Property: {validationError.PropertyName}, Error: {validationError.ErrorMessage}";
                    }
                }
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }
            return mResult;
        }        
    }
}