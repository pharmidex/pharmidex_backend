﻿using PharmidexData.IRepository;
using PharmidexServices.IServices;

namespace PharmidexServices.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repo;

        public UserService(IUserRepository repo)
        {
            _repo = repo;
        }
    }
}