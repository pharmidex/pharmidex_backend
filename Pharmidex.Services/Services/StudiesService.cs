﻿using AmarnaEnergy.IServices;
using AmarnaEnergyData.IRepository;
using Pharmidex.Core.Helpers;
using Pharmidex.Core.Models;
using Pharmidex.Core.Models.Base;
using Pharmidex.Entity;
using PharmidexServices.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Pharmidex.Core.LinqExtensions;
using System.Data.Entity;
using Pharmidex.Core.Enums;
using Pharmidex.Core.Resources;
using System.Data.Entity.Validation;
using System.Text;

namespace AmarnaEnergy.Services
{
    public class StudiesService : IStudiesService
    {
        private readonly IStudiesRepository _repo;

        public StudiesService(IStudiesRepository repo)
        {
            _repo = repo;
        }

        public async Task<ResponseModel> AddUpdate(ManageStudiesModel model)
        {
            return await _repo.AddUpdate(model);
        }

        public async Task<ResponseModel<ManageStudiesModel>> GetDetailsForEdit(int Id)
        {
            ResponseModel<ManageStudiesModel> mResult = new ResponseModel<ManageStudiesModel>()
            {
                Status = ResponseStatus.Success,
                Message = ResponseMessages.OK,
                Result = await (from x in _repo.FindAll(y => y.Id == Id)
                                select new ManageStudiesModel()
                                {
                                    Id = x.Id,
                                    UId = x.UId,
                                    ProposalNo = x.ProposalNo,
                                    CustomerId = x.CustomerId,
                                    ProposalDate = x.ProposalDate,
                                    StudyDetails = (from y in x.StudyDetails
                                                    select new StudyDetailsManageModel()
                                                    {
                                                        Id = y.Id,
                                                        BAEndDate = y.BAEndDate,
                                                        BAStartDate = y.BAStartDate,
                                                        CompletionAuthorisedBy = y.CompletionAuthorisedBy,
                                                        CompletionDate = y.CompletionDate,
                                                        CompoundsRecDate = y.CompoundsRecDate,
                                                        Compounds = y.Compounds,
                                                        DepartmentId = y.DepartmentId,
                                                        DateArchived = y.DateArchived,
                                                        DirectorId = y.DirectorId,
                                                        InvoiceDate = y.InvoiceDate,
                                                        InvoiceNo = y.InvoiceNo,
                                                        Notes = y.Notes,
                                                        ReportDate = y.ReportDate,
                                                        SamplesDescription = y.SamplesDescription,
                                                        SamplesReceivedDate = y.SamplesReceivedDate,
                                                        SamplesShippedDate = y.SamplesShippedDate,
                                                        StudyNo = y.StudyNo,
                                                        StudyStatus = y.StudyStatus,
                                                        StudyTypeId = y.StudyTypeId,
                                                        VIVOEndDate = y.VIVOEndDate,
                                                        VIVOStartDate = y.VIVOStartDate,
                                                    }).ToList()
                                }).FirstOrDefaultAsync()
            };

            if (mResult.Result == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ResponseMessages.NoDataFound;
            }
            return mResult;
        }

        public async Task<ResponseModel> DeleteAsync(IBaseEntityId model)
        {
            ResponseModel mResult = new ResponseModel();

            try
            {
                var studies = await _repo.FindAll(x => x.Id == model.Id).FirstOrDefaultAsync();
                if (studies != null)
                {
                    await _repo.Delete(studies);
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Studies deleted successfully!";
                }
            }
            catch (DbEntityValidationException err)
            {
                StringBuilder sb = new StringBuilder();
                foreach (DbEntityValidationResult eve in err.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        sb.Append($"Property: {ve.PropertyName}, Error: {ve.ErrorMessage} {Environment.NewLine}");
                    }
                }
                mResult.Message = sb.ToString();
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }

            return mResult;
        }

        public async Task<DataTableAjaxResultModel> GetFilteringList(DataTableAjaxPostModel model)
        {
            string searchBy = (model.search != null) ? model.search.value : string.Empty;
            int take = model.length;
            int skip = model.start;
            string sortBy = "CreatedUTC";
            bool sortDir = false;

            if (model.order != null)
            {
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == GlobalConfig.asc;
            }

            Expression<Func<Studies, bool>> whereClause = DynamicWhereClause.BuildDynamicStudiesClause(searchBy);


            List<StudiesDatatableModel> result = await (from q in _repo.FindAll(whereClause)
                                                        select new StudiesDatatableModel
                                                        {
                                                            Id = q.Id,
                                                            CustomerName = q.User.Name,
                                                            ProposalDate = q.ProposalDate,
                                                            ProposalNo = q.ProposalNo,
                                                            Count = q.StudyDetails.Count(),
                                                        }).OrderBy(sortBy, sortDir).Skip(skip).Take(take).ToListAsync();

            if (result == null)
            {
                return new DataTableAjaxResultModel();
            }
            return new DataTableAjaxResultModel()
            {
                draw = model.draw,
                recordsFiltered = await (from q in _repo.FindAll(whereClause) select q.Id).CountAsync(),
                recordsTotal = await _repo.FindAll(null).CountAsync(),
                data = result
            };
        }
    }
}