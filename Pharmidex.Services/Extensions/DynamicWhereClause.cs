﻿using PharmidexEntity;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Pharmidex.Entity;

namespace PharmidexServices.Extensions
{
    public static class DynamicWhereClause
    {
        public static Expression<Func<ApplicationUser, bool>> BuildDynamicApplicationUserClause(string searchValue)
        {
            ExpressionStarter<ApplicationUser> predicate = PredicateBuilder.New<ApplicationUser>(true);
            if (!string.IsNullOrWhiteSpace(searchValue))
            {
                List<string> searchTerms = searchValue.Split(' ').ToList();
                predicate = predicate.Or(x => searchTerms.Any(srch => x.Name.Contains(srch)));
                predicate = predicate.Or(x => searchTerms.Any(srch => x.Email.Contains(srch)));
            }
            return predicate;
        }

        public static Expression<Func<Studies, bool>> BuildDynamicStudiesClause(string searchValue)
        {
            ExpressionStarter<Studies> predicate = PredicateBuilder.New<Studies>(true);
            if (!string.IsNullOrWhiteSpace(searchValue))
            {
                List<string> searchTerms = searchValue.Split(' ').ToList();
                predicate = predicate.Or(x => searchTerms.Any(srch => x.ProposalNo.Contains(srch)));
                predicate = predicate.Or(x => searchTerms.Any(srch => x.Customer.Name.Contains(srch)));
            }
            return predicate;
        }
    }
}