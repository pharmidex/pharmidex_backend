﻿using AmarnaEnergyData.IRepository;
using AmarnaEnergyData.Repository;
using PharmidexData.IRepository;
using PharmidexData.Repository;
using Unity;
using Unity.Extension;

namespace PharmidexServices.Extensions
{
    public class DependencyInjectionExtension : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            Container.RegisterType<IAccountRepository, AccountRepository>();
            Container.RegisterType<IApplicationSettingRepository, ApplicationSettingRepository>();
            Container.RegisterType<IUserRepository, UserRepository>();
            Container.RegisterType<IStudiesRepository, StudiesRepository>();
        }
    }
}